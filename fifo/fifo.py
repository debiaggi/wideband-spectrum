from corr import *
import time
import cmd
import sys


#Connecting and programming
c = katcp_wrapper.FpgaClient("roach3")
while not c.is_connected():
    pass
print "Connected on roach3"

#Settings
dest_ip  =192*(2**24) + 168*(2**16) + 11*(2**8) + 11
dest_port = 7200
pkgDim = 512 #Parole da 64

c.read_uint("fifo")
print "Versione schema : " + str(c.read_uint("version"))
c.write_int("port_dest",dest_port)
time.sleep(0.1)
c.write_int("ip_dest",dest_ip)
time.sleep(0.1)
c.write_int("pkglen",pkgDim)
time.sleep(0.1)
c.write_int("res",1)
time.sleep(0.1)
c.write_int("res",0)
time.sleep(0.5)

#Avvio interfaccia 10gbe per ip 192.168.11.103 (roach3)
c.tap_start("gbe","Gbe", 415549098855 , 3232238439, 7200)
time.sleep(1.0)


class Shell(cmd.Cmd):
    
    def __init__(self, *args, **kwargs):
        """
        Classe shell
        """
        cmd.Cmd.__init__(self, completekey='tab', stdin=None, stdout=sys.stderr)        

    def do_trig(self,arg):
        c.write_int("trig",1)
        time.sleep(0.1)
        c.write_int("trig",0)
    
    def do_stat(self,arg):
        gb_status = c.read_uint("gb_status") 
        print  format( gb_status, '08x')  
    
    def do_reset(self,arg):
        c.write_int("res",1)
        time.sleep(0.5)
        c.write_int("res",0)
        
    def do_exit(self,arg):
        c.tap_stop("Gbe")
        sys.exit(0)
   
def parse(arg):
    'Convert a series of zero or more numbers to an argument tuple'
    return tuple(map(int, arg.split()))     
        
#Ativo shell
s = Shell()
s.cmdloop()
