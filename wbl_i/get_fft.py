#@brief Specializzo la classe program per il compito specifico di raccolta statistiche adc
import datetime
from time import *
import numpy as np
import sys
import struct
import os
import signal
from matplotlib.pylab import subplots,close
from matplotlib import cm
from tables import *
import corr
from collections import *
import cmd
from operator import  add
from format import format

frm = format(128)

# Connection to roach

class Fpga(corr.katcp_wrapper.FpgaClient):
    """
    Metodi di accesso e lavoro sul fpga
    """
    def __init__(self):
        print "Roach3 Connected :"
        super(Fpga, self).__init__("roach3")
        while not self.is_connected():
            sleep(0.2)
        self.ping()
        self.fig, self.ax = subplots(1,1)
    
    def reset(self):
        print "FGPA Scheme reset"
        self.write_int("res",1)
        sleep(0.01)
        self.write_int("res",0)
        self.write_int("fft_out_en",0)
        
        
    def init(self,arg):
        """
        Init fgpa : integrazione , shift , enable , go_fft
        """
        print "Init device.."
        #Lettura info scheda
        self.read_uint("wbs")# Se non e' lo schema giusto da errore!
        self.version = self.read_uint("version")
        self.freq    = self.read_uint("adcfreq")
        self.interleaved = self.read_uint("interleaved")
        self.channels = self.read_uint("channels")
        self.bins = [0]*self.channels
        self.showed = False
        
        #Passaggio parametri scheda
        self.write_int("fft_en",1)
        self.write_int("fft_int",arg)
        self.write_int("fft_shift",170)
        self.write_int("quant_gain",1)
        self.write_int("snap_what",0)
        self.write_int("dbg_frame",0)
        self.write_int("fft_out_en",0)
        self.int = arg
        self.reset()
        #Inserisco l'ora : TO DO dovrei attendere il cambio di un secondo per scrivere ed assicrurarmi che fpga riceva 
        #il dato in quel secondo
        d = datetime.datetime.now()
        self.write_int("in_pos_t",mktime(d.timetuple()))
        print "Load time : " + str(mktime(d.timetuple()))
        self.write_int("initdata",1)
        self.write_int("initdata",0)
        sleep(0.1)
        self.write_int("go_fft",1)
        self.write_int("go_fft",0)
        print "Version : " + str(self.version)
        print "Freq : " + str(self.freq)
        print "Interleaved : " + str(self.interleaved)
        print "Channels: " +str(self.channels)
    
    def shift(self,arg):
        "Setta le divisioni sui vari stage della fft"
        self.roach.write_int("fft_shift",int(arg))
        print "Fft shift now is " + arg        
    
    def gain(self,arg):
        "Setta il guadagno sul quantizzatore"
        self.write_int("quant_gain",int(arg))
        print "Gain now is " + arg        
    
    def integration(self,arg):
        "Setta il numero di accumulazioni"
        self.int=int(arg)
        self.write_int("fft_int",int(arg))
        print "Integraions now are " + arg
    
    def get_data(self,arg):
        """
        Richiedo uno snap e leggo i dati
        get_data [what] [ silent] , what : lo stage da snappare , silent : evita i print
        """
        in_ = parse(arg)
        silent = False
        #check argomenti richiesta
        if in_[0] > 3:
            return
        #Silent ?
        if len(in_) > 1 :
            silent = True   
        self.write_int("snap_what",in_[0])
        stages = {0 : "Integrated fft data",
                  1 : "Power bins",
                  2 : "Raw fft",
                  3 : "pfb"}
        if silent == False : print "Stage requested : " + stages[in_[0]]
        sleep(0.1)    
        self.write_int("fft_snap",1)
        self.write_int("fft_snap",0)
        # Accoradato al tempo di integrazione * 2 , nel caso peggiore devo attendere 
        # la fine dell'integrazione corrente 
        sleep((self.int * self.channels/2 * 5*10**(-9) )* 3)
        data_bram  = self.read("bram_snap", 67*8)
        data_bram_64 = struct.unpack("67L", data_bram)
        if silent == False : print "Bram :"
        self.bins  = struct.unpack(">128I", data_bram[0   : 512])
        posix_time, = struct.unpack(">I", data_bram[512   : 512+4 ])
        sub_time,   = struct.unpack(">I", data_bram[512+4 : 512+8 ])
        overflow,   = struct.unpack(">Q", data_bram[512+8: 512+16])
        id,         = struct.unpack(">Q", data_bram[512+16: 512+24])
        if silent == False : 
            print " Frame Data : "
            frm.display(16,self.bins)
            print "Posix Time seconds : " + str(posix_time)
            print "Time : " + datetime.datetime.fromtimestamp(posix_time).strftime('%Y/%m/%d %H:%M') + "." + str(sub_time)
            print "Overflows : " + bin(overflow)
            print "Overflows : " + str(overflow)
            print "Fft index : " + str(id)
        return self.bins    
         
    def collect(self,arg):
        "Calcolo media on the fly di n campioni di dati ogni 1/2 sec nello stadio scelto"
        "arg 0 n campioni"
        "arg 1 fft stage"
        num_data = 10 # default 10 letture snap
        stage = 1   #default pre quant
        in_ = parse(arg)
        if len(in_) != 2:
            print "Collect need 2 args : num of samples and stage"
            return 
        num_data, stage =in_ 
        print "Collecting "+str(num_data)+" lectures from stage "+ str(stage)
        print "wait.."
        #Collect routine
        #Tengo da parte valori minimi e massimi del range e valore medio
        sum_vector = [0]*self.channels
        min_value = 2^32-1
        max_value = 0
        iter = 0
        while iter < num_data:
            iter += 1
            self.get_data(str(stage)+" 1") #silent get data from selected stage
            #analize max min from bins
            for nch in arange(0,self.channels):
                if self.bins[nch] > max_value:
                    max_value = self.bins[nch]
                if self.bins[nch] < min_value:
                    min_value = self.bins[nch]
            sum_vector = map( add, sum_vector, self.bins )
            sleep(1.0)
        #Eseguo la media
        avg_vector = [x/num_data for x in sum_vector]
        print "Mean over band :"
        print avg_vector
        print "Min Value : " + str(min_value)
        print "Max Value : " + str(max_value)
        self.plot(avg_vector)
    
    def flow(self,arg):
        "Avvia la raccolta continua sulla bram preposta"
        "dell fft integrata"
        "flow sec : secondi di integrazione"
        intnum = float(self.freq*(10**6) / 4)
        intnum = 1/intnum * (self.channels / 2)
        intnum = int(float(arg) / intnum)
        bits = (intnum*256).bit_length() 
        print " Possible max value in bits : " +  str(bits)
        self.integration(str(intnum))
        self.write_int("fft_out_en",1)

    def plot(self,arg):
        """ 
        Plotto i bins
        """
        #Rimuovo la continua
        bins_plot = list(arg)
        bins_plot[0]=0
        print "!! WARNING channel 0 was deleted before plot !!"
        #plot
        if self.showed == False:
            self.xdata = np.arange(0,self.channels)
            self.ax.set_xlim(0,self.channels)
            self.ax.set_ylim(0,max(bins_plot))
            self.fig.canvas.draw()
            self.plt = self.ax.plot(self.xdata,self.bins)[0]
            self.fig.show()
            self.showed = True
        
        self.ax.set_xlim(0,self.channels)
        self.ax.set_ylim(0,max(bins_plot)*15/10 )    
        self.plt.set_data(self.xdata,bins_plot)
        self.fig.canvas.draw()
            
###SHELL INTERFACE CLASS ###

class Shell(cmd.Cmd):
    """
    Shel comandi wideband spectrum
    """
    intro = "Welcome to roach3 wideband spectrum consolle.."
    prompt = "wbs: "
    file = None
    
    def __init__(self, *args, **kwargs):
        """
        Istanza classse di accesso ala roach
        """
        cmd.Cmd.__init__(self, completekey='tab', stdin=None, stdout=sys.stderr)
        #Istanza classe fpga wbs
        self.roach = Fpga()        
        # Ok fai l'init dela scheda
        self.roach.init(3125000)    
    #---- comandi---#
    def do_program(self,arg):
        "Roach download scheme wbl.bof"
        print "Programming wbl.bof.."
        print self.roach.progdev("wbl.bof")
        #reinit scheda
        self.roach.init(100)
    
    def do_listdev(self,arg):
        "Listing scheme availables registers"
        print self.roach.listdev()
    
    def do_init(self,arg):
        "Set up default values for scheme and start internal loop"
        self.roach.init(int(arg))
    
    def do_get(self,arg):
        "Request fft data save , it waits 0.5 sec reads bram and plot"
        self.roach.get_data(arg)
        #self.roach.plot(self.roach.get_data(arg))
    
    def do_gain(self,arg):
        "Change fft gain"   
        self.roach.gain(arg)
        
    def do_shift(self,arg):
        "Change fft stage shifting value"   
        self.roach.gain(arg) 
    
    def do_int(self,arg):
        "Change fft stage shifting value"    
        self.roach.integration(arg)
    
    def do_time(self,arg):
        "Get fpga time"
        print "Posix_time : " +  str(self.roach.read_uint("pos_sec_sample"))
        print "Sub time : " +    str(self.roach.read_uint("pos_ck_sample"))
        
    def do_eval(self,arg):
        "Eseguo il comando passato : non funzia, da sviluppare"
        print eval('1+2')
        
    def do_exit(self,arg):
        "Exit program"
        close(self.roach.fig)
        sys.exit(0)     
    
    def do_dbg(self,arg):
        "Seleziono la linea mux che mi interessa per il debug"
        self.roach.write_int("dbg_sel",int(arg))
        sleep(0.1)
        print self.roach.read_uint("dbg")
    
    def do_timedbg(self,arg):
        "Attivo/disattivo il mux di debug concat time data"
        print "Time frame 0xFF values : " + arg
        self.roach.write_int("dbg_frame",int(arg)) 
    
    def do_syncdbg(self,arg):
        "Seleziono l'input di test per controllare il sync con il dato fft sulla bram"
        print "Selecting data vaid instead of data for bram : " + arg
        self.roach.write_int("dbg_fft",int(arg))
    
    def do_bands(self,arg):
        "Esegue il calcolo dei centri banda secondo il numero dei canali e la banda passante"
        print "Central band channels frequency"
        d = (self.roach.freq*1000000/2)/self.roach.channels
        d2= d/2
        for b in range(0,self.roach.channels):
            print str(b) + " : " + str(d*b) + "Hz"
    
    def do_collect(self,arg):
        "Richiede un campionamento statistico dei on the fly dei dati lungo la catena fft"
        "arg Numero di campioni distanziati di mezzo secondo + stadio"
        self.roach.collect(arg)
    
    def do_flow(self,arg):
        "Avvia il flow continuo in uscita sulla bram preposta"
        self.roach.flow(arg)  
    
        
  
def parse(arg):
    'Convert a series of zero or more numbers to an argument tuple'
    return tuple(map(int, arg.split()))  
    
    
if __name__ == '__main__':
    s = Shell()
    s.cmdloop()
    
    
    
    
