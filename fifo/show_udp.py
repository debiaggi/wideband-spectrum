#!/usr/bin/env python

import SocketServer
import socket
import struct
import datetime
import time
import logging
import logging.handlers
import os
#import corr
import cPickle as pickle

#RICORDA IL TAP START:
#c.tap_start("gbe","Gbe", 415549098855 , 3232238439, 1234)


#Setting up logging    
log_filename = "log/dataserver.log"
logger = logging.getLogger('DataLogger')
logger.setLevel(logging.INFO)
console_log = logging.StreamHandler()
file_log = logging.handlers.RotatingFileHandler(log_filename, maxBytes=8388608, backupCount=5)
formatter = logging.Formatter("%(asctime)s - %(levelname)s: %(message)s", "%j_%H:%M:%S")
console_log.setFormatter(formatter)
file_log.setFormatter(formatter)
logger.addHandler(console_log)
logger.addHandler(file_log)

ip="192.168.11.11"
fpga=[]
roach='roach3'
katcp_port=7147
   
if __name__=="__main__":
    from optparse import OptionParser
    import sys

    #command line parsing
    op = OptionParser()
    op.add_option("-p", "--port", type="int", dest="port", default=7200)
    op.add_option("-f", "--fmt", dest="fmt", default=">Q")
    op.add_option("-o", "--outfile", dest="outfile", default="noname", help="target name")
    op.add_option("-k", "--pkg_len", type="int", dest="pkg_len", default=513, help="gets multiplied by 8")
    op.add_option("-n", "--no_record", dest="no_rec", action="store", default=False, help="don\'t write data to disk")
    op.add_option("-s", "--start_time", dest="start", default="now", help="UTC Time as YYYY-MM-DD_HH:MM:SS")
    op.add_option("-t", "--stop_time", dest="stop", default="never", help="UTC Time as YYYY-MM-DD_HH:MM:SS")
    op.add_option("-x", "--sel_pol", dest="sel_pol", default=0, help="Sel pol. V=0, H=1 (Default 0)")
    opts, args = op.parse_args(sys.argv[:])
    port = opts.port
    fmt = opts.fmt
    #indice = opts.indice
    pkg_len = opts.pkg_len * 8
    #rec = not opts.no_rec 
    rec = True
    outfile = opts.outfile
    start = opts.start
    stop = opts.stop
    if opts.sel_pol == "H":
        sel_pol = 1
    else:
        sel_pol = 0

    #if len(args)==0:
    #    logger.info("No config file specified, exit.")
    #    exit()
    #else:
    #    config=args[0]

    #file_log.doRollover()
    logger.info("Running with options:")
    logger.info("\tport: " + str(port))
    logger.info("\tpkg length: " + str(pkg_len))
    logger.info("\tfmt: " + str(fmt))
    logger.info("\ttarget name: " + outfile)
    logger.info("\tstart time: " + start)
    logger.info("\tstop time: " + stop)
 
        
    if rec:
        out =  open(outfile, "a")

    if start == "now":
        start_time = datetime.datetime.utcnow()
    else:
        start_time = datetime.datetime.strptime(start, "%Y-%m-%d_%H:%M:%S")

    if stop == "never":
        delta = datetime.timedelta(days=365)
        stop_time = start_time + delta
    else:
        stop_time = datetime.datetime.strptime(stop, "%Y-%m-%d_%H:%M:%S")

    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    logger.info("Start time UTC:\t" + str(start_time))
    logger.info(" Stop time UTC:\t" + str(stop_time))

    #t_zero = get_t_zero(config)
    #logger.info("Sync signal has been sent at: "+str(datetime.datetime.utcfromtimestamp(t_zero)))
    #a=input("Digita control+c")
    #print "Writing File Header: "
    #fpga.read_uint('')
    #roach='feng'
    #print('Connecting to ROACH board named "%s"... '%roach),
#     fpga = corr.katcp_wrapper.FpgaClient(roach)
#     time.sleep(.1)
#     if fpga.is_connected():
#         logger.info("Connected to ROACH board named "+roach)
#     else:
#         logger.info('ERROR connecting to server %s on port %i.\n'%(roach,katcp_port))
#         #exit()
    #if sel_pol==0: 
    #    logger.info("Setting Polarization V")
    #else:
    #    logger.info("Setting Polarization H")
    #fpga.write_int('sel_pol',sel_pol)
    #time.sleep(0.01)
    #if fpga.read_uint('sel_pol')==sel_pol:
    #    logger.info("Polarization set successfully")
    #else:
    #    logger.info("ERROR: Polarization Read value ",fpga.read_uint('sel_pol'))
    count = -1

    if start != "now":    
        logger.info("Waiting for start time...")

    while datetime.datetime.utcnow() < start_time:
        time.sleep(0.3) 

    logger.info("Server listening "+ip+":"+str(port))
    server.bind((ip, port))
    lost = 0
    try:
        while datetime.datetime.utcnow() < stop_time:
            
            buf = server.recv(pkg_len)
            new_count = struct.unpack(fmt, buf[:8])[0]
            if not new_count == count + 1:
                if count == -1:
                    logger.info("First packet received " + str(new_count))
                    first_pkt = new_count
                    if rec:
                         logger.info("Recording...")
                    else:
                         logger.info("Not Recording!!!")
                else:
                    logger.error("jumping from " + str(count) + " to " + str(new_count))
                    lost += (new_count-count)
            count = new_count
            if (count % 2000) == 0:
                #print len(buf)
                vettore=struct.unpack('!'+str((pkg_len-8)/8)+'Q', buf[8:])
                for i in range(((pkg_len-8)/8)/(18+16+2)):
                    print "[%d/%d] "%(count,i),
                    #print (vettore[11+i*36] & 0xffff0000) >> 32, vettore[11+i*36] & 0xffff,
                    for j in range (18+16+2):
                        print vettore[(i*36)+j],
                    print ""
                #print count, (vettore[11] & 0xffff0000) >> 32, vettore[11] & 0xffff

            if rec:
                out.write(buf[:])
                out.flush()
            #del out
            #del buf
        logger.info("Last packet received " + str(new_count))
        logger.info("Packets Received: " + str(new_count-first_pkt) + ", Lost: " + str(lost))
        if rec:
            logger.info("Closing file.")
            out.flush()
            out.close()
            del out
            del buf
        logger.info("Closing Comunication.")
        logger.info("Ended Successfully")

    except KeyboardInterrupt:
        logger.info("Last packet received " + str(new_count))
        logger.info("Packets Received: " + str(new_count-first_pkt) + ", Lost: " + str(lost))
        if rec:
            logger.info("Closing file.")
            out.flush()
            out.close()
            del out
            del buf
        logger.info("Closing Comunication.")
        logger.info("Ended Successfully")


