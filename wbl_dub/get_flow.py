#@brief Leggo di continuo la bram flow dallo schema wbs
import datetime
from time import *
import numpy as np
import sys
import struct
import os
import signal
from matplotlib.pylab import subplots,close
from matplotlib import cm
from tables import *
from corr import  *
from symbol import except_clause
from reg import *

"""
Usage python get_flow.py nomefile_senzaestensione stream[0..n-1]
Interrompi con ctrl C
"""

relative_include_path = '/home/debiaggi/git/wideband-spectrum' 
sys.path.insert(0, relative_include_path)
from format import format

#Controllo argomenti
if ( len( sys.argv ) < 2 ):
    print "Hey ! hdf file ??"
    sys.exit(0)
    
#Selezione stream  
stream = 0   
if( len( sys.argv) >= 3):
    stream  =  int(sys.argv[2])
#parametro di test velocita di lettura
test_speed = 0
if ( len( sys.argv) >= 4):
        test_speed = int(sys.argv[3])

#Process vars
last_id = 0
process = True
num_ex = 0
#Classe formattazione dati su consolle
frm = format(1024)    
#Global var
c=0
i=0
f=0
ch=0
integ=0

#Client scheda roach
try:
    #Board client
    c = katcp_wrapper.FpgaClient("roach3")
    while not c.is_connected():
        pass
    i = c.read_uint("interleaved")
    f = c.read_uint("adcfreq")
    ch = c.read_uint("channels")
    integ = c.read_uint(regs[stream]['fft_int'])
    gain = c.read_uint(regs[stream]['quant_gain'])
    shift = c.read_uint(regs[stream]['fft_shift'])
except RuntimeError:
    print "Roach not responding..exiting"
    sys.exit(0)

#Apertura file hdf per salvataggio da argomento 1
if not os.path.exists(sys.argv[1]+".hdf5"):
    print "ATTENZIONE IL FILE " + sys.argv[1] + " NON ESISTE! "
    print "-> CREO IL NUOVO FILE"
try:
    # Apro / creo il file hdf    
    file = openFile(sys.argv[1] +".hdf5", mode = "a", title = "Spectrometer data")
except:
    print "Error on accessing hdf file..exiting"
    sys.exit(0)
    
# Creo il gruppo per quest'acquisizione + gli attributi
group = file.createGroup('/', "time" + strftime("%Y_%m_%d_%H_%M_%S"), 'Fft channels from fpga spectrometer')    
f = float(f)
integ = float(integ)
int_time = (integ*ch/2)/(f*(10**6)/4)
print "INT time : " + str(int_time)
print "Stream : " + str(stream)
file.setNodeAttr(group,"Bandwith","800")
file.setNodeAttr(group,"Channels",str(ch))
file.setNodeAttr(group,"IntergrationTime",str(int_time))
file.setNodeAttr(group,"Qunatization gain",str(gain))
file.setNodeAttr(group,"FFT shift",str(shift))
#Creo la descrizione per la tabella  dati
class FlowFields(IsDescription):
    stream = StringCol(1)   #Stream di lavoro
    time = UInt32Col()     #Posix time in secondi
    subtime = UInt32Col()  #Sub istanti di clock all'interno del secondo
    overflow = UInt32Col() # Bit di overflow da adc / fft
    id = UInt64Col()       #Id successivo di questo spettro
    channels = UInt32Col(ch)#Valori dei rispettivi canali  
#Creao la tabella dati
table  = file.createTable(group,'Data',FlowFields,"Spectrometer acquisition data")
row = table.row

try:
    id_bram = c.read(regs[stream]['bram_flow'],8,ch*4+2*16)
    last_id, = struct.unpack(">Q" ,id_bram)
    print "First id : " + str(last_id)
except:
    print "Error reading first id from roach"

# Loop di lettura / salvataggio
try:
    while 1:
        try:
            #Lettura bram, per l'acquisizione ne basta una
            process = True 
            id_bram = c.read(regs[stream]['bram_flow'],8, (ch/4) * 16 + 32 )
            id, = struct.unpack(">Q" ,id_bram)
            sleep(0.1)
        except RuntimeError:            
            process = False
            num_ex +=1
        
        if process == True:                        
            sys.stdout.write("\rId " + str(id) + " / exc : " + str(num_ex))
            sys.stdout.flush()
            if id != last_id :
                data_bram  = c.read(regs[stream]['bram_flow'], (ch/4 ) * 16 + 3*16  )
                if test_speed > 0:  #Speed test per controllare se leggo i dati prima del prossimo frame
                    id_bram = c.read(regs[stream]['bram_flow'],8, (ch/4) * 16 + 32 )
                    idt, = struct.unpack(">Q" ,id_bram)
                    if idt == id:
                        print "ACQUISITION OK"
                    else:
                        print "ACQUISITION FAIL"
                else:            
                    bins  = struct.unpack(">1024I", data_bram[0   : ch*4])
                    posix_time, = struct.unpack(">I", data_bram[ch*4     : ch*4 + 4 ])
                    sub_time,   = struct.unpack(">I", data_bram[ch*4 + 4 : ch*4 + 8 ])
                    overflow,   = struct.unpack(">I", data_bram[ch*4 + 16: ch*4 +16 + 4 ])
                    id,         = struct.unpack(">Q", data_bram[ch*4 + 32: ch*4 +32 + 8  ])
                    print "----------------"
                    print "Posix Time seconds : " + str(posix_time)
                    print "Time : " + datetime.datetime.fromtimestamp(posix_time).strftime('%Y/%m/%d %H:%M') + "." + str(sub_time)
                    print "Overflows : " + bin(overflow)
                    print "Fft index : " + str(id)
                    bins_plot = list(bins)
                    bins_plot[0]=0
                    frm.display( 16, bins_plot )
                    #Salvataggio dati su hdf
                    row['stream']  = stream
                    row['time']    = posix_time
                    row['subtime'] = sub_time
                    row['overflow']= overflow
                    row['id']      = id
                    row['channels']= bins_plot
                    row.append()
                    table.flush()
                    #ax.set_ylim(0,max(bins_plot)*150/100  )  
                    #plt.set_data(xdata,bins_plot)  
                    #fig.canvas.draw()                
            last_id = id
            
except KeyboardInterrupt:
    #close(fig)
    file.close()
    sys.exit(0)
    
