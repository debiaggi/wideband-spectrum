#!/usr/bin/env python
import SocketServer
import socket
import struct
import datetime
from time import  *
import os
import cPickle as pickle
from format import format
import sys
from tables import *

#avvio : python get_udp /datafile(senza estensione) streamnum(0-1) 

#RICORDA IL TAP START:
#c.tap_start("gbe","Gbe", 415549098855 , 3232238439, 1234)

ip="192.168.11.11"
fpga=[]
roach='roach3'
katcp_port=7147
pkg_len = 519 * 8  
hdfFile = 0
c=0
i=0
f=0
ch=0
integ=0
#Controllo argomenti
if ( len( sys.argv ) < 2 ):
    print "Hey ! hdf file ??"
    sys.exit(0)
#Selezione stream  
stream = 0   
if( len( sys.argv) >= 3):
    stream  =  int(sys.argv[2])
#parametro di test velocita di lettura
test_speed = 0
if ( len( sys.argv) >= 4):
        test_speed = int(sys.argv[3])

# Formattazione dati a video
frm = format(1024)
rec = True
count = -1
lost = 0
received = 0

#Apertura file hdf per salvataggio da argomento 1
if not os.path.exists(sys.argv[1]+".hdf5"):
    print "ATTENZIONE IL FILE " + sys.argv[1] + " NON ESISTE! "
    print "-> CREO IL NUOVO FILE"
try:
    # Apro / creo il file hdf    
    file = open_file(sys.argv[1] +".hdf5", mode = "a", title = "Spectrometer data")
except:
    print "Error on accessing hdf file..exiting"
    sys.exit(0)

# Creo il gruppo per quest'acquisizione + gli attributi
group = file.create_group('/', "time" + strftime("%Y_%m_%d_%H_%M_%S"), 'Fft channels from fpga spectrometer')    
file.set_node_attr(group,"Bandwith","800")
file.set_node_attr(group,"Channels","1024")
file.set_node_attr(group,"IntergrationTime","0.5")
file.set_node_attr(group,"QntGain","1")
file.set_node_attr(group,"FFTShift","0")
#Creo la descrizione per la tabella  dati
class FlowFields(IsDescription):
    stream = StringCol(1)   #Stream di lavoro
    time = UInt32Col()     #Posix time in secondi
    subtime = UInt32Col()  #Sub istanti di clock all'interno del secondo
    overflow = UInt32Col() # Bit di overflow da adc / fft
    id = UInt64Col()       #Id successivo di questo spettro
    channels = UInt32Col(1024)#Valori dei rispettivi canali  
#Creao la tabella dati
table  = file.create_table(group,'Data',FlowFields,"Spectrometer acquisition data")
row = table.row

# Socket ricezione pacchetto
server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server.bind((ip, 7200))
#Ciclo di acuisizione da socket
try:
    while 1:
        buf = server.recv(pkg_len)
        #Lettura contatore sul primo long del pacchetto ( messo da med
        # packetizer )
        new_count = struct.unpack(">Q", buf[:8])[0]
        if not new_count == count + 1:
            if count == -1:
                first_pkt = new_count
            else:
                print "PACKET LOST!"
                lost += (new_count-count)
        count = new_count
        buf_data= buf[8:]
        #print len(buf_data)
        vettore       = struct.unpack('>1024I', buf_data[0           : 1024*4    ])
        fft_time,     = struct.unpack('>I'    , buf_data[1024*4      : 1024*4 + 4])
        fft_sub_time, = struct.unpack('>I'    , buf_data[1024*4 + 4  : 1024*4 + 8])
        fft_overflow, = struct.unpack('>I'    , buf_data[1024*4 + 16 : 1024*4 + 20])
        fft_id,       = struct.unpack('>Q'    , buf_data[1024*4 + 32 : 1024*4 + 40])   
        #frm.display(16,vettore)
        #print "FPGA Time : " \
        #         + datetime.datetime.fromtimestamp(fft_time).strftime('%Y/%m/%d %H:%M:%S') \
        #         + "." + str(fft_sub_time)        
        #print "Overflow : " + str(fft_overflow)
        #print "Id : " + str(fft_id)
        #Salvataggio dati su hdf    
        #sys.stdout.write("ID " + str(fft_id) +" - LOST " + str(lost) +"- CNT " + str(received )+ "\r" )
        #sys.stdout.flush()
        row['stream']  = stream
        row['time']    = fft_time
        row['subtime'] = fft_sub_time
        row['overflow']= fft_overflow
        row['id']      = fft_id
        row['channels']= vettore
        received += 1
        print "{0}\r ID " + str(fft_id) +" - LOST " + str(lost) +"- CNT " + str(received )
        row.append()
        table.flush()

except KeyboardInterrupt:
    print "LOST PACKETS : " + str(lost)
    file.close()
    sys.exit(0)
