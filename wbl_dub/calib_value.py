import sys
import time




def TuneADC0(client , what):
    """
    Invio i parametri calcolati dal modello di calibrazione all'adc 0
    client : ref al client
    what : 1 set / 0 reset
    """
    # Parametri dalla calibrazione automatica
    offset_vi = 134
    offset_vq = 45
    gc_v = 69
    fisda_v = 0 #esadecimale ?
    
    # Sblocco i registri adc
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x0,0x0,0x03,0x0))
    # Inserisco le correzioni per l'offset'
    
    # No calibration mode
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x60,0x6c,0x0,0x01),offset=0x4)
    time.sleep(0.05) # probably unnecessary wait for delay to take
    #Resetto il cotroller
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x0,0x0,0x03,0x03))

    # Azzero i valori 
    client.blindwrite('iadc_controller','%c%c%c%c'%(offset_vi,offset_vq,0x02,0x01),offset=0x4)
    time.sleep(0.001) # probably unnecessary wait for delay to take
    #Resetto il cotroller
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x0,0x0,0x03,0x03))
    #gain
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x80,0x80,0x01,0x1),offset=0x4)
    time.sleep(0.001) # probably unnecessary wait for delay to take
    #Resetto il cotroller
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x0,0x0,0x03,0x03))
    # gain  compensation
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x00,0x00,0x03,0x01),offset=0x4)
    time.sleep(0.001) # probably unnecessary wait for delay to take
    #Resetto il cotroller
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x0,0x0,0x03,0x03))
    
    # Solo reset ?
    if what == 0:
        return
    
    #OFFSET
    time.sleep(0.1)
    # from calibration : i 134 - q 45
    client.blindwrite('iadc_controller','%c%c%c%c'%(offset_vq,offset_vi,0x02,0x01),offset=0x4)
    time.sleep(0.05)
    #Resetto il cotroller
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x0,0x0,0x03,0x03))
    
    # GUADAGNO
    time.sleep(0.1)
    client.blindwrite('iadc_controller','%c%c%c%c'%(gc_v,gc_v,0x03,0x01),offset=0x4)
    time.sleep(0.05)
    #Resetto il cotroller
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x0,0x0,0x03,0x03))
    
    # DELAY
    time.sleep(0.1)
    drda_i = 0
    drda_q = 0
    b=((fisda_v&0x3)<<6)+(drda_q<<3)+drda_i     
    a=fisda_v>>2
    client.blindwrite('iadc_controller','%c%c%c%c'%(a,b,0x07,0x01),offset=0x4)
    time.sleep(0.05)
    #Resetto il cotroller
    client.blindwrite('iadc_controller','%c%c%c%c'%(0x0,0x0,0x03,0x03))
    