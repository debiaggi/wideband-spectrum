

class format:
    """
    Classe formata dati a schermo
    """
    def __init__(self,channels):
        self.channles = channels
        
    def display(self,l,data):
        """
        Formatta una semplice tabella sullo schermo
        l : lunghezza linea
        data : array dati
        """
        for i in range(0,self.channles/l):
            print "FMT channels" + str(self.channles)
            line = [0]*l
            for ii in range(0 , l):
                line[ii] = data[(i*l) + ii ]
            print str(l*i) + " to " + str(i*l+l-1) +" : " + str(line) 
