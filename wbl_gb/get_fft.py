#@brief Specializzo la classe program per il compito specifico di raccolta statistiche adc
import datetime
from time import *
import numpy as np
import sys
import struct
import os
import signal
import matplotlib.pyplot as plt
from matplotlib import cm
from tables import *
import corr
from collections import *
import cmd
from operator import  add
from reg import  *
relative_include_path = '/home/debiaggi/git/wideband-spectrum' 
sys.path.insert(0, relative_include_path)
from format import format
import calib_value
import gb


#Ausilio formattazione canali in consolle
frm = format(1024)
#Color
ENDC = '\033[0m'
FAIL = '\033[91m'
OKGREEN = '\033[92m'
fpga_freq= 200000000

# Connection to roach

class Fpga(corr.katcp_wrapper.FpgaClient):
    """
    Metodi di accesso e lavoro sul fpga
    """
    def __init__(self):
        print "Roach3 Connected :"
        super(Fpga, self).__init__("roach3")
        while not self.is_connected():
            sleep(0.2)
        self.ping()
        self.stream = 0
        
    def select_stream(self,arg):
        "Seleziona lo streaming su cui lavorare"
        if int(arg)  < self.num_stream + 1: 
            self.stream = int(arg)
            print "Selected stream " + str(self.stream) + " [ first is 0 ]" 
            print "Streams available : " + str(self.num_stream) 
        else :
            print "Stream non present!!"    
            
    def reset(self):
        print "FGPA Scheme reset"
        self.write_int("res",1)
        sleep(0.01)
        self.write_int("res",0)
        self.write_int('fft_out_en',0)
    
    def read_params(self):
        "Leggo i parametri sul canale selezionato"
        print "Actual stream : " + str(self.stream)
        print "Shift : " + str(self.read_uint(regs[self.stream]['fft_shift']))
        print "Gain : " + str(self.read_uint(regs[self.stream]['quant_gain']))
        print "Integrations : " + str(self.read_uint(regs[self.stream]['fft_int']))
        print "Continuos flow : " + str(self.read_uint("fft_out_en"))
        print "Snap What : " + str(self.read_uint(regs[self.stream]['snap_what']))
        print "Time on fpga : "\
         + datetime.datetime.fromtimestamp(self.read_uint("pos_sec_sample")).strftime('%Y/%m/%d %H:%M:%S')

    def calibration(self,arg):
        "Setto/resetto i valori di calibrazione "
        print "Calibration values : "+ arg
        calib_value.TuneADC0(self,int(arg))
                
    def init(self,arg):
        """
        Init fgpa : integrazione , shift , enable , go_fft
        """
        print "Write calibrated values to adc 0 register.."
        calib_value.TuneADC0(self, 1)
        
        print "Init device.."
        #Lettura info scheda
        self.read_uint("wbl_gb")# Se non e' lo schema giusto da errore!
        self.version        = self.read_uint("version")
        self.freq           = self.read_uint("adcfreq")
        self.interleaved    = self.read_uint("interleaved")
        self.channels       = self.read_uint("channels")
        self.num_stream     = self.read_uint("streams_out")
        self.bins   = [0]*self.channels
        self.showed = False
        
        for  stream in regs:
            #Passaggio parametri dfault  per entrambi i canali
            self.write_int(stream['fft_snap'],0)
            self.write_int(stream['fft_en'],1)
            self.write_int(stream['fft_int'],self.set_integration_time(arg))
            self.write_int(stream['fft_shift'],0)
            self.write_int(stream['quant_gain'],1)
            self.write_int(stream['snap_what'],0)
            self.write_int(stream['dbg_frame'],0)
        #Integrazone di default    
        self.int = arg
        self.reset()
        #Inserisco l'ora : TO DO dovrei attendere il cambio di un secondo per scrivere ed assicrurarmi che fpga riceva 
        #il dato in quel secondo
        d = datetime.datetime.now()
        self.write_int("in_pos_t",mktime(d.timetuple()))
        print "Load time : " + str(mktime(d.timetuple()))
        self.write_int("initdata",1)
        self.write_int("initdata",0)
        self.avg_vector = [0]*self.channels
        self.sum_vector = [0]*self.channels
        print "Version : " + str(self.version)
        gb.init_gb0(self)
        print "Freq : " + str(self.freq)
        print "Interleaved : " + str(self.interleaved)
        print "Channels: " +str(self.channels)
        print "Number of streams : " + str(self.num_stream)
        print "Selected stream : " + str(self.stream)
        self.trig_fft()
        gb.gb0_start(self)
        frm = format(self.channels)
        self.read_params()
        
    
    def trig_fft(self):
        "Rido il trig fft"
        sleep_time = (self.int * self.channels/4 * 5*10**(-9) )* 3
        if sleep_time < 0.99:
            sleep_time = 1
        sleep(sleep_time)
        self.write_int("go_fft",1)
        self.write_int("go_fft",0)
    
    def shift(self,arg):
        "Setta le divisioni sui vari stage della fft"
        in_ = parse(arg)
        self.write_int(regs[self.stream]['fft_shift'],int(arg))
        print "Fft shift on stream " + str(self.stream) + " now is " + arg        
    
    def gain(self,arg):
        in_  = parse(arg)
        if len(in_)< 1:
            gn = 1
        else:
            gn = in_[0]    
        "Setta il guadagno sul quantizzatore"
        self.write_int(regs[self.stream]['quant_gain'], gn)
        print "Gain on stream " + str(self.stream) + " now is " + str(gn)        
    
    def set_integration_time(self,arg):
        "Ritorna il numero di itegrazioni per il tempo richiesto"
        intnum = float(self.freq*(10**6) / 4)
        intnum = 1/intnum * (self.channels / 4)
        intnum = int(float(arg) / intnum)
        return intnum
    
    def integration(self,arg):
        "Setta il numero di accumulazioni"
        integ = self.set_integration_time(arg)
        self.write_int(regs[self.stream]['fft_int'],integ)
        print "Integraions now are " + str(integ)
        bits = (integ*256).bit_length() 
        print " Possible max value in bits : " +  str(bits)
        
    def get_data(self,arg):
        """
        Richiedo uno snap e leggo i dati
        get_data [what] [ silent] , what : lo stage da snappare , silent : evita i print
        """
        in_ = parse(arg)
        silent = False
        if len(in_) == 0:
            return
        #check argomenti richiesta
        if in_[0] > 4:
            return
        #Silent ?
        if len(in_) > 2 :
            silent = True   
    
        stages = {0 : "Integrated fft data",
                  1 : "Post Quantization",
                  2 : "Power bins",
                  3 : "Raw fft",
                  4 : "pfb"}
        if silent == False : print "Stage requested : " + stages[in_[0]]     
        #Seleziono la linea dati richiesta  
        self.bins = [0]*self.channels
        self.write_int(regs[self.stream]['snap_what'],in_[0])
        sleep(0.1)
        #Leggo l'id attuale.attendo che l'id sia cambiato
        if in_[0]==0:
            id_bram = self.read(regs[self.stream]['bram_snap'],8,self.channels*4+2*16)
            id_bram_start, = struct.unpack(">Q" ,id_bram)
            print "id_bram_start : " + str(id_bram_start)
            #Richiedo lo snap quando l'id e cambiato
            self.write_int(regs[self.stream]['fft_snap'],1)
            self.write_int(regs[self.stream]['fft_snap'],0)
            #Attendo la fine dell'integrazione corrente 
            wait_timeout = time()
            while 1:
                if time() > wait_timeout + 10:
                    print FAIL + "Timeout occured waiting new frame" + ENDC
                    print "\n"
                    return
                id_bram = self.read(regs[self.stream]['bram_snap'],8,self.channels*4+2*16)
                id_bram_stop, = struct.unpack(">Q" ,id_bram)
                sys.stdout.write("\rId " + str(id_bram_stop) )
                sys.stdout.flush()
                if  id_bram_stop != id_bram_start:
                    print "\n"
                    print OKGREEN + "Got new frame, reading data..." + ENDC
                    break  
        else:
            print OKGREEN + "Reading data..." + ENDC
            #Richiedo lo snap quando l'id e cambiato
            self.write_int(regs[self.stream]['fft_snap'],1)
            self.write_int(regs[self.stream]['fft_snap'],0)            
            sleep(0.1)
                
        #Ok..nuovo frame in bram!    
        data_bram  = self.read(regs[self.stream]['bram_snap'], (self.channels/4 ) * 16 + 3*16  )
        self.bins  = struct.unpack(">1024I", data_bram[0   : self.channels*4])
        self.posix_time, = struct.unpack(">I", data_bram[self.channels*4     : self.channels*4 + 4 ])
        self.sub_time,   = struct.unpack(">I", data_bram[self.channels*4 + 4 : self.channels*4 + 8 ])
        self.overflow,   = struct.unpack(">I", data_bram[self.channels*4 + 16: self.channels*4 +16 + 4 ])
        self.id,         = struct.unpack(">Q", data_bram[self.channels*4 + 32: self.channels*4 +32 + 8  ])
        if silent == False : 
            print "Stream " + str(self.stream) + " : "
            cleaned_bins = list(self.bins)
            cleaned_bins[0] = -1
            frm.display(16,cleaned_bins)
            print "Fpga Time : "  \
                   + datetime.datetime.fromtimestamp(self.posix_time).strftime('%Y/%m/%d %H:%M:%S') \
                   + "." + str(self.sub_time)
            print "Overflows : " + bin(self.overflow)
            print "Fft index : " + str(self.id)
        return self.bins    
         
    def collect(self,arg):
        "Calcolo media on the fly di n campioni di dati ogni 1/2 sec nello stadio scelto"
        "arg 0 n campioni"
        "arg 1 fft stage"
        num_data = 10 # default 10 letture snap
        stage = 1   #default pre quant
        in_ = parse(arg)
        if len(in_) != 2:
            print "Collect need 2 args : num of samples and stage"
            return 
        num_data, stage =in_ 
        print OKGREEN + "Collecting "+str(num_data)+" lectures from stage "+ str(stage) +" at "+  strftime("%H:%M:%S") + ENDC
        print "wait.."
        #Collect routine
        #Tengo da parte valori minimi e massimi del range e valore medio
        min_value = 2^32-1
        max_value = 0
        iter = 0
        passed = 0
        self.sum_vector = [0]*self.channels
        while iter < num_data:
            iter += 1
            self.get_data(str(stage)+" 1") #silent get data from selected stage
            #Non devo essere in overflow
            if self.overflow != 0:
                passed += 1
                continue
            #analize max min from bins
            for nch in np.arange(0,self.channels):
                #Se il canale e marchiato come escluso non lo calcolo
                if self.bins[nch] == -1 or nch == 0:
                    continue
                #..Canale buono
                if self.bins[nch] > max_value:
                    max_value = self.bins[nch]
                if self.bins[nch] < min_value:
                    min_value = self.bins[nch]
                    if min_value == 0:
                        print FAIL + "MIN VALUE 0 " + ENDC
            self.sum_vector = map( add, self.sum_vector, self.bins )
        #Eseguo la media
        for i in np.arange(0,self.channels):
            if self.sum_vector[i] > 0 :
                self.avg_vector[i] = (self.sum_vector[i]/(num_data - passed)).bit_length()
            else:
                self.avg_vector[i] = 0  
        self.avg_vector[0] = 0
        print "\n---------RESULTS---------"
        print "Mean expressed in bits over band for " + str(num_data) + " acquisitions :"
        print frm.display(16,self.sum_vector)
        print frm.display(16,self.avg_vector)
        print "Min Value : " + str(min_value) + " - Bits : " + str(min_value.bit_length())
        print "Max Value : " + str(max_value) + " - Bits : " + str(max_value.bit_length())
        if passed > 0:
            print FAIL + "ATTENTION : Encountered " +str(passed) + " overflows !" + ENDC
        self.plot(self.avg_vector)
        
    
    def flow(self,arg):
        "Avvia la raccolta continua sulla bram preposta"
        "dell fft integrata"
        "flow sec : secondi di integrazione"
        #Disabilito uscita + disabilito abilitazione startup
        self.write_int("fft_out_en",0)
        self.write_int("st_abil",0)
        if arg != "stop":
            self.write_int("fft_out_en",0)
            self.integration(arg)
            self.trig_fft()
            sleep(0.1)
            self.write_int("fft_out_en",1)

    def plot(self,arg):
        """ 
        Plotto i bins
        """
        #Rimuovo la continua
        bins_plot = list(arg)
        bins_plot[0]=0
        print "!! WARNING channel 0 was deleted before plot !!"
        x_data = np.arange(0,self.channels)
        plt.plot( x_data, bins_plot )
        plt.show()
        
    def bands(self,arg):
        print "Central band channels frequency"
        if self.interleaved:
            d = (self.freq*1000000)/self.channels
        else:
            d = (self.freq/2*1000000)/self.channels   
        d2= d/2
        for b in range(0,self.channels):
            print str(b) + " : " + str(d*b) + "Hz"
            
    def pkg_len(self,arg):
        "Cambio la lunghezza del pacchetto gb ed eseguo restart"
        self.write_int("fft_out_en",0)
        self.write_int("pkg_len",int(arg))
        self.trig_fft()
        
    def startat(self,arg):
        "Invio all'fpga l'orario per la partenza programmata"
        "L'orario viene in posix float.I decimali sono convertiti in cloks subtime"
        self.write_int("fft_out_en",0)
        pos_float = float(arg)
        pos_sec, pos_sub = divmod(pos_float, 1)
        if pos_sub > (1/(self.freq/4*1000000.0)):
            pos_sub = pos_sub/(1/(self.freq/4*1000000.0))
        else:
            pos_sub = 1    
        print pos_sec
        print pos_sub    
        self.write_int("st_sec", int(pos_sec))
        self.write_int("st_sub", int(pos_sub))
        self.write_int("st_abil",1)
            
###SHELL INTERFACE CLASS ###

class Shell(cmd.Cmd):
    """
    Shel comandi wideband spectrum
    """
    intro = "Welcome to roach3 wideband spectrum consolle.."
    prompt = "wbl_dub: "
    file = None
    
    def __init__(self, *args, **kwargs):
        """
        Istanza classse di accesso ala roach
        """
        cmd.Cmd.__init__(self, completekey='tab', stdin=None, stdout=sys.stderr)
        #Istanza classe fpga wbs
        self.roach = Fpga()        
        # Ok fai l'init dela scheda
        self.roach.init(0.5)    
    #---- comandi---#
    def do_program(self,arg):
        "Roach download scheme wbl.bof"
        print "Programming wbl.bof.."
        print self.roach.progdev("wbl.bof")
        #reinit scheda
        self.roach.init(0.5)
    
    def do_listdev(self,arg):
        "Listing scheme availables registers"
        print self.roach.listdev()
    
    def do_init(self,arg):
        "Set up default values for scheme and start internal loop"
        self.roach.init(int(arg))
    
    def do_read(self,arg):
        "legge i parametri dello strema selezionato"
        self.roach.read_params()
        
    def do_reset(self,arg):
        self.roach.reset()
    
    def do_trig(self,arg):
        "Rida il trig alle catene fft"
        self.roach.trig_fft()
    
    def do_get(self,arg):
        "Request fft data save , it waits 0.5 sec reads bram and plot"
        self.roach.get_data(arg)
    
    def do_gain(self,arg):
        "Change fft gain"   
        self.roach.gain(arg)
        
    def do_shift(self,arg):
        "Change fft stage shifting value"   
        self.roach.shift(arg) 
    
    def do_int(self,arg):
        "Change fft stage shifting value"    
        self.roach.integration(arg)
    
    def do_time(self,arg):
        "Get fpga time"
        print "Time on fpga : "\
         + datetime.datetime.fromtimestamp(self.roach.read_uint("pos_sec_sample")).strftime('%Y/%m/%d %H:%M:%S')
        
        
    def do_eval(self,arg):
        "Eseguo il comando passato : non funzia, da sviluppare"
        print eval('1+2')
        
    def do_exit(self,arg):
        "Exit program"
        gb.gb0_stop(self.roach)
        sys.exit(0)     
    
    def do_dbg(self,arg):
        "Seleziono la linea mux che mi interessa per il debug"
        self.roach.write_int(regs[self.stream]['dbg_sel'],int(arg))
        sleep(0.1)
        print self.roach.read_uint(regs[self.stream]['dbg'])
    
    def do_timedbg(self,arg):
        "Attivo/disattivo il mux di debug concat time data"
        print "Time frame 0xFF values : " + arg
        self.roach.write_int(regs[self.stream]['dbg_frame'],int(arg)) 
    
    def do_syncdbg(self,arg):
        "Seleziono l'input di test per controllare il sync con il dato fft sulla bram"
        print "Selecting data vaid instead of data for bram : " + arg
        self.roach.write_int(regs[self.stream]['dbg_fft'],int(arg))
    
    def do_bands(self,arg):
        "Esegue il calcolo dei centri banda secondo il numero dei canali e la banda passante"
        self.roach.bands(arg)
    
    def do_collect(self,arg):
        "Richiede un campionamento statistico dei on the fly dei dati lungo la catena fft"
        "arg Numero di campioni distanziati di mezzo secondo + stadio"
        self.roach.collect(arg)
    
    def do_flow(self,arg):
        "Avvia il flow continuo in uscita sulla bram preposta"
        self.roach.flow(arg)  
        
    def do_select(self,arg):
        "Richiedo il cambio di stream"
        self.roach.select_stream(arg)
  
    def do_cal(self,arg):
        """
        Agisce sulla calibrazione : 1 setta i valori corretti / 0 mette tutto a 0 
        """
        self.roach.calibration(arg)
    
    def do_plot(self,arg):
        "Plotta gli utlimi bins letti tramite il get"
        self.roach.plot(self.roach.bins)
        
    def do_gbstat(self,arg):
        "Recupero i registri di stato e debug della gb0"
        gb0_stat,gb0_debug = gb.gb0_status(self.roach)
        print "Gb0 status :" + hex(gb0_stat)
        print "Gb0 debug :" + hex(gb0_debug) 
        
    def do_pkglen(self,arg):
        "Modifica la lunghezza del pkg seriale"
        print "New pkg_len is : " + arg
        self.roach.pkg_len(arg)
        
    def do_startat(self,arg):
        "Pianifica la partenza da posix con decimale"
        self.roach.startat(arg)
            
def parse(arg):
    'Convert a series of zero or more numbers to an argument tuple'
    return tuple(map(int, arg.split()))  
    
    
if __name__ == '__main__':
    s = Shell()
    s.cmdloop()
    
    
    
    
