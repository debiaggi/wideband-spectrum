#Ausilio accesso registri specifici
regs_i = {  'name'      : 'i',
            'snap_what' : 'i1024_snap_what',
            'quant_gain': 'i1024_quant_gain',
            'flow_dbg'  : 'i1024_flow_dbg',
            'fft_snap'  : 'i1024_fft_snap',
            'fft_shift' : 'i1024_fft_shift',
            'fft_ov'    : 'i1024_fft_ov',
            'fft_out_en': 'i1024_fft_out_en',
            'fft_int'   : 'i1024_fft_int',
            'fft_id'    : 'i1024_fft_id',
            'fft_id_snap':'i1024_fft_id_snap',
            'fft_en'    : 'i1024_fft_en',
            'dbg_sel'   : 'i1024_dbg_sel',
            'dbg_frame' : 'i1024_dbg_frame',
            'dbg'       : 'i1024_dbg',
            'bram_flow' : 'i1024_bram_flow',
            'bram_snap' : 'i1024_bram_snap',
            'adc_q_ov'  : 'i1024_adc_q_ov',
            'adc_i_ov'  : 'i1024_adc_i_ov'}


regs = [ regs_i  ] 