#Ausilio accesso registri specifici
regs_i = {  'name'      : 'i',
            'snap_what' : 'i128_snap_what',
            'quant_gain': 'i128_quant_gain',
            'flow_dbg'  : 'i128_flow_dbg',
            'fft_snap'  : 'i128_fft_snap',
            'fft_shift' : 'i128_fft_shift',
            'fft_ov'    : 'i128_fft_ov',
            'fft_out_en': 'i128_fft_out_en',
            'fft_int'   : 'i128_fft_int',
            'fft_id'    : 'i128_fft_id',
            'fft_id_snap':'i128_fft_id_snap',
            'fft_en'    : 'i128_fft_en',
            'dbg_sel'   : 'i128_dbg_sel',
            'dbg_frame' : 'i128_dbg_frame',
            'dbg'       : 'i128_dbg',
            'bram_flow' : 'i128_bram_flow',
            'bram_snap' : 'i128_bram_snap',
            'adc_q_ov'  : 'i128_adc_q_ov',
            'adc_i_ov'  : 'i128_adc_i_ov'}


regs = [ regs_i ]   