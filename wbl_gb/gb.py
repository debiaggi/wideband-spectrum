#Init periferica gigabit
#Il reset viene effettuato del trigger dell'fft
#Successivamente con fft_out_en abiliti l'uscita delle integrazioni verso la gb0
import time

#Settings
dest_ip  =192*(2**24) + 168*(2**16) + 11*(2**8) + 11
dest_port = 7200
pkgDim = 518 #Parole da 64

def init_gb0(roach):
    """
    Inizializza i registri della gb0
    Non esegue il reset!!!!
    """
    roach.write_int("Gb0_port_dest",dest_port)
    print "GB0 dest port : " + str(dest_port)
    time.sleep(0.1)
    roach.write_int("Gb0_ip_dest",dest_ip)
    print "GB0 dest ip : " + str(dest_ip)
    time.sleep(0.1)
    roach.write_int("pkg_len",pkgDim)
    print "Gb0 packet len : " + str(pkgDim)
    time.sleep(0.1)
    
def gb0_start(roach):
    "Avvio interfaccia 10gbe per ip 192.168.11.103 (roach3)"
    roach.tap_start("Gb0","Gb0_Gbe", 415549098855 , 3232238439, 7200)
    time.sleep(0.5)   
    
def gb0_status(roach):
    """
    Legge i registri di stato / debug della gigabit
    """
    status = roach.read_uint("gb0_status")
    debug = roach.read_uint("gb0_debug")
    return status, debug

def gb0_stop(roach):
    "Stoppo la gb0"
    roach.tap_stop("Gb0")